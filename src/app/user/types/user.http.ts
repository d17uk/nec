// Shared
import { UserGender, UserStatus } from "./user";

export interface UserRecordInterface {
  id: string,
  fields: UserRecordFieldsInterface,
  createdTime: string;
}

export interface UserRecordFieldsInterface {
  'Date of birth': string,
  'First name': string,
  'Last name': string,
  'Gender': UserGender,
  'Status': UserStatus
}

// Requests
export interface CreateUserRequestInterface {
  fields: UserRecordFieldsInterface
}

// Responses
export interface UserListResponseInterface {
  records: UserRecordInterface[],
  offset: string
}
