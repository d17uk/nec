// Enums
export enum UserStatus {
  ACTIVE = 'Active',
  SUSPENDED = 'Suspended'
}

export enum UserGender {
  MALE = "Male",
  FEMALE = "Female",
  NOT_SPECIFIED = "Not specified"
}

// Interfaces
export interface UserDataInterface {
  dateOfBirth: Date;
  firstName: string;
  lastName: string;
  gender: UserGender;
  status: UserStatus;
}

export interface UserInterface extends UserDataInterface {
  id: string;
  createdTime: Date;
}

export interface UserListInterface {
  users: UserInterface[];
  offset: string;
}

