import { Component, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { UserDataInterface} from "../../types/user";
import { UserHttpService} from "../../services/user.http";
import { FormCreateUserComponent} from "../../forms/create-user/create-user.form";

@Component({
  selector: 'nec-create-user',
  templateUrl: './create-user.component.html'
})
export class CreateUserComponent {

  @ViewChild('form') createUserForm!: FormCreateUserComponent;

  public submissionError: boolean = false;
  public submissionSuccess: boolean = false;

  constructor(
    private router: Router,
    private userHttp: UserHttpService
  ) {
  }

  public showUserList(): void {
    this.router.navigate(['/','users']);
  }

  public createUser(userData: UserDataInterface): void {
    this.createUserForm.toggleLock();
    this.submissionError = false;
    this.submissionSuccess = false;
    this.userHttp.createUser(userData)
      .subscribe((response) => {
        this.createUserForm.toggleLock();
        this.submissionError = response === false;
        this.submissionSuccess = !this.submissionError;
        if (this.submissionSuccess) {
          this.createUserForm.resetForm();
        }
      });
  }

}
