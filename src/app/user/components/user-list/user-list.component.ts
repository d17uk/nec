import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { Router } from "@angular/router";

import { UserListInterface } from "../../types/user";
import { UserStore } from "../../services/user.store";

@Component({
  selector: 'nec-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {

  public userList$: Observable<UserListInterface|false|null>;

  constructor(
    private userStore: UserStore,
    private router: Router
  ) {
    this.userList$ = this.userStore.userList$;
  }

  public showCreateUser() {
    this.router.navigate(['users','create']);
  }

}
