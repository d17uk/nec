import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import {NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerModule, NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { BootstrapIconsModule } from "ng-bootstrap-icons";
import { CalendarDateFill } from "ng-bootstrap-icons/icons";

import { UserListComponent } from "./components/user-list/user-list.component";
import { USER_ROUTES } from "./user.routing";
import { UserHttpService } from "./services/user.http";
import { CreateUserComponent } from "./components/create-user/create-user.component";
import {NecDateAdapter, NecDateParserFormatter} from "../shared/adapters/datepicker.adapter";
import {FormCreateUserComponent} from "./forms/create-user/create-user.form";

const icons = {
  CalendarDateFill
}

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(USER_ROUTES),
    NgbModule,
    NgbDatepickerModule,
    BootstrapIconsModule.pick(icons)
  ],
  declarations: [
    UserListComponent,
    CreateUserComponent,
    FormCreateUserComponent
  ],
  providers: [
    {
      provide: NgbDateAdapter,
      useClass: NecDateAdapter
    },
    {
      provide: NgbDateParserFormatter,
      useClass: NecDateParserFormatter
    }
  ],
  exports: [
    FormCreateUserComponent
  ]
})
export class UserModule {
  constructor(private userHttp: UserHttpService) {
    userHttp.loadUsers().subscribe();
  }
}
