import { Routes } from "@angular/router";
import { UserListComponent } from "./components/user-list/user-list.component";
import { CreateUserComponent } from "./components/create-user/create-user.component";

export const USER_ROUTES: Routes = [
  {
    path: '',
    component: UserListComponent
  },
  {
    path: 'create',
    component: CreateUserComponent
  }
]
