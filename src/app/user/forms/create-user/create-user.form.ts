import {Component, EventEmitter, Output, ViewChild} from "@angular/core";
import { UserDataInterface, UserGender, UserStatus } from "../../types/user";
import {FormGroup, Validators, FormBuilder, AbstractControl, FormGroupDirective} from "@angular/forms";

@Component({
  selector: 'nec-form-create-user',
  templateUrl: './create-user.form.html',
  styleUrls: ['./create-user.form.scss']
})
export class FormCreateUserComponent {

  @Output() formSubmit = new EventEmitter<UserDataInterface>();

  @ViewChild('form') formGroupDirective?: FormGroupDirective;

  public lock: boolean = false;

  public createUserForm: FormGroup = this.cleanForm();

  constructor(
    private fb: FormBuilder
  ) {
    this.cleanForm();
  }

  public onSubmit(): void {
    if (this.createUserForm.valid) {
      let formValues = {
        ...this.createUserForm.value,
        dateOfBirth: new Date(this.createUserForm.controls['dateOfBirth'].value)
      }
      this.formSubmit.emit(formValues);
    }
  }

  get userGenders(): string[] {
    return Object.values<string>(UserGender);
  }

  get userStatuses(): string[] {
    return Object.values<string>(UserStatus);
  }

  get controls(): { [key: string]: AbstractControl; } {
    return this.createUserForm.controls;
  }

  public toggleLock(): void {
    this.lock = !this.lock;
  }

  public resetForm(): void {
    this.formGroupDirective?.resetForm();
    this.createUserForm = this.cleanForm();
  }

  private cleanForm(): FormGroup {
    return this.createUserForm = this.fb.group({
      dateOfBirth: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: ['', Validators.required],
      status: ['', Validators.required]
    })
  }

}
