import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { catchError, map, Observable, switchMap, tap } from "rxjs";

import { UserStore } from "./user.store";
import {
  CreateUserRequestInterface,
  UserListResponseInterface, UserRecordFieldsInterface,
  UserRecordInterface
} from "../types/user.http";
import {
  UserInterface,
  UserDataInterface,
  UserListInterface
} from "../types/user";
import { ErrorService } from "../../shared/services/error.service";


@Injectable({
  providedIn: 'root'
})
export class UserHttpService {

  private apiUrl = environment.apiUrl + 'Users';

  constructor(
    private http: HttpClient,
    private userStore: UserStore,
    private errorService: ErrorService
  ) {
  }

  get url(): string {
    return this.apiUrl;
  }

  public loadUsers(): Observable<void|false> {
    this.userStore.clearList();
    return this.http.get<UserListResponseInterface>(this.apiUrl)
      .pipe(
        map((userListData: UserListResponseInterface) => this.transformUserListData(userListData)),
        tap(userList => this.userStore.setList(userList)),
        switchMap(() => new Observable<void>()),
        catchError(this.errorService.handleError)
      );
  }

  public createUser(newUser: UserDataInterface): Observable<UserInterface|false> {
    const requestData = this.createUserRequestData(newUser);
    return this.http.post<UserRecordInterface>(this.apiUrl, requestData)
      .pipe(
        map((userData: UserRecordInterface) => this.transformUserData(userData)),
        tap(() => this.loadUsers().subscribe()),
        catchError(this.errorService.handleError)
      );
  }

  private transformUserListData(response: UserListResponseInterface): UserListInterface {
    return {
      users: response.records.map(this.transformUserData),
      offset: response.offset
    }
  }

  private transformUserData(userData: UserRecordInterface): UserInterface {
    return {
      id: userData.id,
      dateOfBirth: new Date(userData.fields['Date of birth']),
      firstName: userData.fields['First name'],
      lastName: userData.fields['Last name'],
      gender: userData.fields['Gender'],
      status: userData.fields['Status'],
      createdTime: new Date(userData.createdTime)
    };
  }

  private createUserRequestData(userData: UserDataInterface): CreateUserRequestInterface {
    const dob = userData.dateOfBirth;
    let fields: UserRecordFieldsInterface = {
      'Date of birth': `${dob.getFullYear()}-${dob.getMonth()+1}-${dob.getDate()}`,
      'First name': userData.firstName,
      'Last name': userData.lastName,
      'Gender': userData.gender,
      'Status': userData.status
    };
    return {
      fields
    }
  }

}
