import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { UserListInterface } from "../types/user";

@Injectable({
  providedIn: 'root'
})
export class UserStore {

  private userListSubject = new BehaviorSubject<UserListInterface|false|null>(null);

  public userList$ = this.userListSubject.asObservable();

  public setList(list: UserListInterface|false): void {
    this.userListSubject.next(list);
  }

  public clearList(): void {
    this.userListSubject.next(null);
  }

}
