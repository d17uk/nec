import { Injectable } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { Observable, of } from 'rxjs'
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  public handleError(error: HttpErrorResponse): Observable<false> {
    if (!environment.production) {
      console.error(error);
    }
    return of(false);
  }

}
